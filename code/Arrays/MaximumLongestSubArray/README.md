# Maximum Longest Subarray

```
Input:
array = [1, 2, 3, 4, 5, 6, 7, 8]
target_sum = 15

Output: (0, 4)

Explanation:
array[0] to array[4] = [1, 2, 3, 4, 5] is the longest subarray whose sum equals to target_sum (15)
```

# Code

```python
def max_longest_subarray(array, sum_value):
    left = 0
    right = 0
    s = 0
    range_value = (left, right)

    while (right < len(array)):
        if s <= sum_value:
            if right - left > range_value[1] - range_value[0]:
                range_value = (left, right-1)
            s = s + array[right]
            right += 1
        else:
            s -= array[left]
            left += 1

    return range_value


if __name__ == "__main__":
    array = [1, 2, 3, 4, 5, 6, 7, 8]
    sum_value = 15

    print(max_longest_subarray(array, sum_value))
```

## Work by

Satshree

# Solution

- Given, `array = [1, 2, 3, 4, 5, 6, 7, 8]`
- Given, `target_sum = 15`

For this problem, you will have to look through the array using pointers _(not exactly pointers, but values that point to the index array's elements)_. In the code, these pointers are `left` and `right`. These pointers will point to the values accordingly through the loop with conditions applied. The conditions will be discussed later below.

```
SET left = 0
SET right = 0
SET sum = 0
SET range = left, right
WHILE right < LENGTH(array):
    IF sum <= SUM_TARGET:
        IF right - left > range(right) - range(left):
            UPDATE range = left, right-1
        ADD array[right] TO sum
        ADD 1 TO right
    ELSE:
        SUBTRACT array[left] FROM sum
        ADD 1 TO left
```

Here,

1. `left` and `right` start pointing at the beginning of the array (i.e `0`).
2. The loop will run until `right` reaches to the end of an array (`right < array.length`)
3. If sum (`s`) is less than `target_sum`, then,

   - If `right - left` (length of answer array) `>` existing range, then update the range with current value of `right` and `left`. This is because in this point in loop, the `right` and `left` range while the sum (`s`) is equal to `target_sum` is of maximum length.

   - Add the `array[right]` value to sum(`s`) and shift the `right` pointer to next right element (increase `right` by `1`). Be careful to execute this after above step to not mess up `right`, `left` and sum (`s`) values.

4. If sum (`s`) exceeds `target_sum`, then,

   - Subtract `array[left]` value from sum (`s`). This is because in next step, the left pointer will be increased and that left out value should be taken out from the sum (`s`).

   - Shift the `left` pointer to its next right element (increase `left` by 1).

So, the main gist is that, these `left` and `right` pointer will determine the longest sub array as long as sum (`s`) is equal to `target_sum`.

As the loop goes, the `right` pointer shifts right as long as sum(`s`) is less than `target_sum` and `left` pointer shifts right as long as sum(`s`) exceeds `target_sum`.

Before shifting `right` pointer, you should check if length of range (`left`, `right`) is long enough to validate as longest subarray. If not, the existing range will be final answer.
