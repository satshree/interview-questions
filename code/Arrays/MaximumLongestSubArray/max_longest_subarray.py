"""
Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.

A subarray is a contiguous part of an array.
"""


def max_longest_subarray(array, target_sum):
    left = 0
    right = 0
    s = 0
    range_value = (left, right)

    while (right < len(array)):
        if s <= target_sum:
            if right - left > range_value[1] - range_value[0]:
                range_value = (left, right-1)
            s = s + array[right]
            right += 1
        else:
            s -= array[left]
            left += 1

    return range_value


if __name__ == "__main__":
    array = [1, 2, 3, 4, 5, 6, 7, 8]
    target_sum = 15

    print(max_longest_subarray(array, target_sum))
