# Two Sum

Given an array of integers `nums` and an integer `target`, return indices of the two numbers such that they add up to `target`.

You may assume that each input would have **exactly one solution**, and you may not use the same element twice.

You can return the answer in any order.

[LeetCode](https://leetcode.com/problems/two-sum/)

# Code

```python
def twoSum(nums, target):
    pairs = []
    table = {}

    for i in range(len(nums)):
        difference = target - nums[i]
        if difference in table:
            # TO APPEND INDICES
            pairs.append(
                (nums.index(difference), i)
            )

            # TO APPEND VALUES
            # pairs.append((table[target - nums[i]], nums[i]))
        table[nums[i]] = i

    return pairs


if __name__ == "__main__":
    array = [1, 2, 3, 4, 5]
    target = 6

    print(twoSum(array, target))
```

## Work by

Satshree

# Solution

- Given array `nums = [1, 2, 3, 4, 5]`
- Given target `6`

_Solution to achieve:_

```
Return indices of two elements from the array whose sum is equal to the target.
```

- Initialize an empty array `pairs` which will contain the pair of elements from `nums` that add up to `target`.
- Initialize an empty hash table (`dict`) which will store the different between `target` and iterated element (`nums[i]`). Use this difference to find the pair.
- If the difference exists in hash table, append to `pairs`.
- Add the difference to hash table.

```
Target(T) = 6
First Element of an Array(F) = 1
Last Element of an Array(L) = 5
Other Elements of an Array = nth

Here,
T = F + L

That also implies,
F = T - L
L = T - F

Which means,
The above two conditions meet in first and last iteration.
The program will return (F, L).
Since each difference of T - nth is kept in hash table,
the program will return the values where nth = F AND nth = L.

```

## OUTPUT

```
[(1, 3), (0, 4)]
```

## Time complexity

O(n)

## Solution and Explanation links

[https://www.educative.io/edpresso/how-to-implement-the-two-sum-problem-in-python](https://www.educative.io/edpresso/how-to-implement-the-two-sum-problem-in-python)

[https://levelup.gitconnected.com/two-sum-interview-problem-in-python-c1d84b029d35](https://levelup.gitconnected.com/two-sum-interview-problem-in-python-c1d84b029d35)
