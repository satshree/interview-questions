"""
Given an array of integers 'nums' and an integer 'target', return indices of the two numbers such that they add up to 'target'.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

You can return the answer in any order.
"""

__author__ = "Satshree Shrestha"


def twoSum(nums, target):
    pairs = []
    table = {}

    for i in range(len(nums)):
        difference = target - nums[i]
        if difference in table:
            # TO APPEND INDICES
            pairs.append(
                (nums.index(difference), i)
            )

            # TO APPEND VALUES
            # pairs.append((table[target - nums[i]], nums[i]))
        table[nums[i]] = i

    return pairs


if __name__ == "__main__":
    array = [1, 2, 3, 4, 5]
    target = 6

    print(twoSum(array, target))
