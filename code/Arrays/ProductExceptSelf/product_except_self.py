"""
Given an integer array nums, return an array answer such that answer[i] is equal to the product of all the elements of nums except nums[i].

The product of any prefix or suffix of nums is guaranteed to fit in a 32-bit integer.

You must write an algorithm that runs in O(n) time and without using the division operation.
"""

__author__ = "Satshree Shrestha"


def productExceptSelf(array):
    # INITIALIZE THE ANSWER ARRAY WITH 1
    answer = [1 for i in array]

    # MULTIPLYING LEFT ELEMENTS LIKE A FACTORIAL
    # START MULTIPLYING FROM 1; SKIP FIRST ELEMENT
    for i in range(1, len(array)):
        # KEEP IN MIND THAT answer[i-1] CONTAINS CUMULATIVE PRODUCT OF LEFT ELEMENTS UNTIL [i-1]
        # FOR INDEX i, IT IS BEING MULITPLIED BY array[i-1] WHICH IS THE VERY NEXT ELEMENT TO THE LEFT
        answer[i] = answer[i - 1] * array[i - 1]

    # START LOOP FROM LAST FOR RIGHT ELEMENTS.
    # START MULTIPLYING FROM 1; SKIP LAST ELEMENT

    r = 1
    for i in range(len(array)-1, -1, -1):
        # answer HAS CUMULATIVE PRODUCT OF LEFT ELEMENTS FOR GIVEN INDEX, MULTIPLY THAT WITH r WHICH HAS CUMULATIVE PRODUCT OF RIGHT ELEMENTS FOR GIVEN INDEX
        answer[i] = answer[i] * r

        # r is the cumulative product for right elements until i+1 for index i.
        # This line is similar to a = a + 1, just that this line is executed from backwards of array.
        r = r * array[i]

    return answer


if __name__ == "__main__":
    array = [1, 2, 3, 4]
    print(productExceptSelf(array))
