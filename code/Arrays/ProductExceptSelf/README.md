# Product of all elements except self

Given an integer array `nums`, return _an array_ `answer` _such that_ `answer[i]` _is equal to the product of all the elements of_ `nums` _except_ `nums[i]`.

The product of any prefix or suffix of `nums` is **guaranteed** to fit in a **32-bit** integer.

You must write an algorithm that runs in `O(n)` time and without using the division operation.

```
Example 1:

Input: nums = [1,2,3,4]
Output: [24,12,8,6]


Example 2:

Input: nums = [-1,1,0,-3,3]
Output: [0,0,9,0,0]
```

[LeetCode](https://leetcode.com/problems/product-of-array-except-self/)

# Code

```python
def productExceptSelf(array):
    answer = [1 for i in array]

    for i in range(1, len(array)):
        answer[i] = answer[i - 1] * array[i - 1]

    r = 1
    for i in range(len(array)-1, -1, -1):
        answer[i] = answer[i] * r
        r = r * array[i]

    return answer


if __name__ == "__main__":
    array = [1, 2, 3, 4]
    print(productExceptSelf(array))
```

## Pseudocode

```
CREATE answer = [1, 1, 1, 1, ... n]

FOR i IN answer:
    # answer[i-1] is the cumulative product of left elements until array[i-1]
    MULTIPLY answer[i-1] WITH array[i-1] THEN
        INSERT TO answer[i]

CREATE r = 1
FOR i IN answer FROM BEHIND:
    # multiply cumulative product of left with cumulative product of right
    MULTIPlY answer[i] WITH r

    # calculating cumulative product of right elements
    MULTIPLY r WITH array[1]
```

## Work by

Satshree

# Solution

This is a very tricky problem. So the solution is commented right above the code lines.

Consider this line,

_Finding out cumulative product of left elements_

```python
answer[i] = answer[i - 1] * array[i - 1]
```

Keep in mind that `answer[i-1]` contains cumulative product of left elements until `[i-1]`

For index `i`, it is being multiplied by `array[i-1]` which is the very next element to the left of `array[i]`.

So now, `answer[i]` contains cumulative product of elements of `array[0]` to `array[i-1]`.

#

Consider this line,

_Multiply cumulative product of left elements with cumulative product of right elements_

```python
answer[i] = answer[i] * r
```

`answer` has cumulative product of left elements for given index `i`, multiply that with `r` which has cumulative product of right elements for given index `i`

#

Consider this line,

_Finding out cumulative product of right elements_

```python
r = r * array[i]
```

`r` is the cumulative product for right elements until `i+1`
for index `i`. This line is similar to `a = a + 1`, just that this line is executed from backwards of array.

## OUTPUT

```
Input:  [1, 2, 3, 4]
Output: [24, 12, 8, 6]
```

## Time Complexity

```
O(n)
```
