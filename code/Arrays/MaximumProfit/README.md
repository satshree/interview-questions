# Maximum Profit

You are given an array `prices` where `prices[i]` is the price of a given stock on the `ith` day.

You want to maximize your profit by choosing a **single day** to buy one stock and choosing a **different day in the future** to sell that stock.

Return _the maximum profit you can achieve from this transaction_. If you cannot achieve any profit, return `0`.

```
Example 1

Input: prices = [7,1,5,3,6,4]
Output: 5
Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6), profit = 6-1 = 5.
Note that buying on day 2 and selling on day 1 is not allowed because you must buy before you sell.


Example 2

Input: prices = [7,6,4,3,1]
Output: 0
Explanation: In this case, no transactions are done and the max profit = 0.
```

[LeetCode](https://leetcode.com/problems/best-time-to-buy-and-sell-stock/)

# Code

```python
def maxProfit(stocks, buy_day):
    buy_day -= 1
    today_price = stocks[buy_day]
    max_profit = 0
    max_profit_day = buy_day
    max_stock = 0

    for i in range(buy_day, len(stocks)):
        if stocks[i] > max_stock:
            max_stock = stocks[i]
            max_profit = abs(stocks[i] - today_price)
            max_profit_day = i + 1

    if max_profit > today_price:
        return {
            "day": max_profit_day,
            "profit": max_profit,
            "stock": max_stock,
        }
    else:
        return {
            "day": 0,
            "profit": 0,
            "stock": today_price
        }


if __name__ == "__main__":
    stocks = [7, 1, 5, 3, 6, 4]
    buy_day = 2

    for key, value in maxProfit(stocks, buy_day).items():
        print(key, "\t", value)
```

## Work by

Satshree

# Solution

Given, `stocks = [7, 1, 5, 3, 6, 4]`

Given, `buy_day = 2`

Calculate _Sell Day, which is, **on day of maximum profit (MaximumFutureStock - BuyingDayStock)**._ This condition is

```
IF (MaximumFutureStock - BuyingDayStock > BuyingDayStock)
    RETURN Day of MaximumFutureStock
ELSE
    RETURN 0 # Since there is no profit
```

## OUTPUT

```
day      5
profit   5
stock    6
```
