"""
You are given an array prices where prices[i] is the price of a given stock on the ith day.

You want to maximize your profit by choosing a single day to buy one stock and choosing a different day in the future to sell that stock.

Return the maximum profit you can achieve from this transaction. If you cannot achieve any profit, return 0.
"""

__author__ = "Satshree Shrestha"


def maxProfit(stocks, buy_day):
    buy_day -= 1
    today_price = stocks[buy_day]
    max_profit = 0
    max_profit_day = buy_day
    max_stock = 0

    for i in range(buy_day, len(stocks)):
        if stocks[i] > max_stock:
            max_stock = stocks[i]
            max_profit = abs(stocks[i] - today_price)
            max_profit_day = i + 1

    if max_profit > today_price:
        return {
            "day": max_profit_day,
            "profit": max_profit,
            "stock": max_stock,
        }
    else:
        return {
            "day": 0,
            "profit": 0,
            "stock": today_price
        }


if __name__ == "__main__":
    stocks = [7, 1, 5, 3, 6, 4]
    buy_day = 2

    for key, value in maxProfit(stocks, buy_day).items():
        print(key, "\t", value)
