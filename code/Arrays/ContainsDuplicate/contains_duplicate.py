"""
Given an integer array nums, return true if any value appears at least twice in the array, and return false if every element is distinct.
"""

__author__ = "Satshree Shrestha"


def containsDuplicateUsingSets(array):
    array_set = set(array)
    return not len(array) == len(array_set)


def containsDuplicate(array):
    hash_table = {}

    for item in array:
        if item in hash_table:
            return True

        hash_table[item] = item

    return False


if __name__ == "__main__":
    array = [1, 2, 3, 4, 1]
    print(containsDuplicate(array))
