# Contains Duplicate

Given an integer array `nums`, return `true` if any value appears **at least twice** in the array, and return `false` if every element is distinct.

```
Example 1

Input: nums = [1,2,3,1]
Output: true


Example 2

Input: nums = [1,2,3,4]
Output: false
```

[LeetCode](https://leetcode.com/problems/contains-duplicate/)

# Code

```python
def containsDuplicateUsingSets(array):
    """ Alternate way by using set """
    array_set = set(array)
    return not len(array) == len(array_set)


def containsDuplicate(array):
    hash_table = {}

    for item in array:
        if item in hash_table:
            return True

        hash_table[item] = item

    return False


if __name__ == "__main__":
    array = [1, 2, 3, 4, 1]
    print(containsDuplicate(array))
```

## Work by

Satshree

# Solution

- Given, `array = [1, 2, 3, 4, 1]`

```
LET HashTable = {}

FOR item IN array:
    IF item EXISTS IN HashTable:
        RETURN True

    INSERT HashTable[item] = item

RETURN False
```

Here, the algorithm is using hash table (`dict`). For every iterated item, it is mapped in hash table. In doing so, the algorithm checks if the item was already added to the hash table before. For unique integers, this condition will always be `False` as they were never added before. If duplicate item is found, the condition will return `True`.

## Alternate Solution

Here, the algorithm uses `set` function. This function removes duplicate items from the array. So if there is a duplicate item in an array, the length of an array will not equal to the set of that array.
