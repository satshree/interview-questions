"""
Given an array nums containing n distinct numbers in the range [0, n], 

return the only number in the range that is missing from the array.
"""

__author__ = "Satshree Shrestha"


def missingNumber(nums, num_range):
    for i in range(0, num_range + 1):
        if i not in nums:
            return i

    return None


if __name__ == "__main__":
    num_range = 9
    nums = [9, 6, 4, 2, 3, 5, 7, 0, 1]

    print(missingNumber(nums, num_range))
