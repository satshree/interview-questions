# Reverse Bits

Reverse bits of a given 32 bits unsigned integer.

```
Input: n = 00000010100101000001111010011100
Output:    964176192 (00111001011110000010100101000000)
Explanation: The input binary string 00000010100101000001111010011100 represents the unsigned integer 43261596, so return 964176192 which its binary representation is 00111001011110000010100101000000.
```

# Code

```python
def missingNumber(nums, num_range):
    for i in range(0, num_range + 1):
        if i not in nums:
            return i

    return None
```

## Work by

Satshree
