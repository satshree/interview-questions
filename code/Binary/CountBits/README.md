# Counting Bits

Given an integer `n`, return *an array* `ans` *of length* `n + 1` *such that for each* `i` `(0 <= i <= n)`, `ans[i]` *is the **number of*** `1` ***'s** in the binary representation of* `i`.

Constraint
```
0 <= n <= 105
```

```
Input: n = 5
Output: [0,1,1,2,1,2]
Explanation:
0 --> 0
1 --> 1
2 --> 10
3 --> 11
4 --> 100
5 --> 101
```

# Code

```python
def countBits(num):
    arr = []

    for i in range(num + 1):
        arr.append(str(bin(i)).count('1'))

    return arr


if __name__ == "__main__":
    num = 5

    print(countBits(num))
```

## Work by

Satshree

# Solution
```python
arr.append(str(bin(i)).count('1'))
```
This is the main line that does everything. `bin` converts the index number to binary, and `count` counts the occurrence of `1` in that binary number.