"""
Given an integer n, return an array ans of length n + 1 such that for each i (0 <= i <= n), 

ans[i] is the number of 1's in the binary representation of i.
"""

__author__ = "Satshree Shrestha"


def countBits(num):
    arr = []

    for i in range(num + 1):
        arr.append(str(bin(i)).count('1'))

    return arr


if __name__ == "__main__":
    num = 5

    print(countBits(num))
